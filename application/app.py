from flask import Flask, render_template, redirect, url_for, request, jsonify, make_response, Response
import sqlite3, bcrypt, pickle, os, ssl, cgi, traceback
from os import popen
from lxml import etree
from base64 import b64decode,b64encode
from passlib.hash import sha256_crypt
from flask_jwt_extended import JWTManager, jwt_required, create_access_token,get_jwt_identity

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'This is the s3cr3t key!'
jwt = JWTManager(app)

_DB = 'class_db.db'
_HASH_SALT = bcrypt.gensalt()

def create_environment():
    try:
        db_conn = sqlite3.connect(_DB)
        print("db connection successful")
        db_conn.execute('DROP TABLE users')
        db_conn.execute('DROP TABLE tickets')
        db_conn.execute('CREATE TABLE IF NOT EXISTS users (first_name TEXT, last_name TEXT, user_name TEXT, password TEXT, permissions TEXT, auth_token TEXT)')
        db_conn.execute('CREATE TABLE IF NOT EXISTS tickets (ticket_title TEXT, ticket_content TEXT, user_name TEXT)')
        print("table created successfully")
        db_cur = db_conn.cursor()
        password = "password123"
        password = bcrypt.hashpw(password, _HASH_SALT)
        db_cur.execute('INSERT INTO users (first_name,last_name,user_name,password,permissions) VALUES ("Ben", "Wireman", "ben.wireman", "{0}", "admin")'.format(password))
        db_cur.execute('INSERT INTO users (first_name,last_name,user_name,password,permissions) VALUES ("Craig", "Mitchell", "craig.mitchell", "{0}", "stduser")'.format(password))
        db_cur.execute('INSERT INTO users (first_name,last_name,user_name,password,permissions) VALUES ("Greg", "Jackson", "greg.jackson", "{0}", "admin")'.format(password))
        db_cur.execute('INSERT INTO tickets (ticket_title,ticket_content,user_name) VALUES ("Ticket 01", "Need password reset", "greg.jackson")')
        db_cur.execute('INSERT INTO tickets (ticket_title,ticket_content,user_name) VALUES ("Ticket 02", "Requesting new computer", "ben.wireman")')
        db_cur.execute('INSERT INTO tickets (ticket_title,ticket_content,user_name) VALUES ("Ticket 03", "Purchase new asset for project", "ben.wireman")')
        db_conn.commit()
        print("users created successfully")
        print("tickets created successfully")
        db_conn.close()
        print("setup and ready to go")
    except:
        print("Error in setup")

# A1 and A2
@app.route('/login', methods=['GET','POST'])
def login():
    error = None

    if request.method == "GET":
        return """
        <html>
           <body>
              <form action = "/login" method = "POST">
                 <p><h3>Login</h3></p>
                 <input type='text' name="username" value="Username"/><br/><br/>
                 <input type='text' name="password" value="Password"/>
                 <p><input type='submit' value='Login'/></p>
              </form>
           </body>
        </html>
        """

    else:
        req_user = request.form['username']
        req_pass = request.form['password']
        req_pass = bcrypt.hashpw(req_pass.encode('utf-8'), _HASH_SALT)
        print("user: {} pass: {}".format(req_user, req_pass))
        
        # A1 BROKEN
        # test" OR "1=1"--
        req = 'select * from users where user_name = "' + req_user + '" AND password = "' + req_pass + '"'
        print(req)
        with sqlite3.connect(_DB) as db_conn:
            db_cursor = db_conn.cursor()
            db_cursor.execute(req)
            rows = db_cursor.fetchall()
            print(rows)

        # A1 FIXED
        # req = 'select * from users where user_name = ? AND password = ?'
        # with sqlite3.connect(_DB) as db_conn:
        #     db_cursor = db_conn.cursor()
        #     db_cursor.execute(req, [req_user, req_pass])
        #     rows = db_cursor.fetchall()
        #     print(rows)
        
        if len(rows) > 0:
            # A2 BROKEN - https://jwt.io/
            # JWTs - tokens need to be protected (Session management)
            access_token = create_access_token(identity="{0};{1};{2}".format(req_user,req_pass,rows[0][4]))
            return jsonify({"token": access_token}), 200
        else:
            return jsonify({"message": "problem with auth"}), 400

@app.route('/users', methods=['GET'])
@jwt_required
def get_all_users():
    current_user = get_jwt_identity()
    permissions = current_user.split(';')[-1]
    users = None
    req = 'select * from users'
    with sqlite3.connect(_DB) as db_conn:
        db_cursor = db_conn.cursor()
        db_cursor.execute(req)
        rows = db_cursor.fetchall()
        users = rows
        print(rows)

    return jsonify({"users": users}), 200

# A3
@app.route('/client', methods=['GET','POST'])
def create_client():
    error = None
    if request.method == "GET":
        return """
        <html>
           <body>
              <form action = "/client" method = "POST">
                 <p><h3>Enter Client Info</h3></p>
                 <input type='text' name="client_name" value="Client Name"/><br/><br/>
                 <input type='text' name="client_ssn" value="Client SSN"/>
                 <p><input type='submit' value='Submit'/></p>
              </form>
           </body>
        </html>
        """
    else:
        req_client_name = request.form['client_name']
        req_client_ssn = request.form['client_ssn']
        # A3 FIX
        # req_client_ssn = bcrypt.hashpw(req_client_ssn.encode('utf-8'), _HASH_SALT)
        print("client: {} ssn: {}".format(req_client_name, req_client_ssn))
        return jsonify({"client": req_client_name, "ssn": req_client_ssn, "message": "Client created successfully"}), 200

# A4
"""
<!DOCTYPE foo [
  <!ELEMENT foo ANY>
  <!ENTITY bar SYSTEM
  "file:///etc/shadow">
]>
<foo>
  &bar;
</foo>
"""
@app.route('/xml', methods = ['POST', 'GET'])
def xml():
    parsed_xml = None
    if request.method == 'POST':
        xml = request.form['xml']
        # A4 BROKEN
        parser = etree.XMLParser(no_network=False, dtd_validation=True)
        # A4 FIX
        # parser = etree.XMLParser(no_network=False, dtd_validation=False, resolve_entities=False)
        try:
            doc = etree.fromstring(str(xml), parser)
            parsed_xml = etree.tostring(doc)
        except:
           pass
    return """
    <html>
       <body>""" + "Result:\n<br>\n" + cgi.escape(parsed_xml) if parsed_xml else "" + """
          <form action = "/xml" method = "POST">
             <p><h3>Enter xml to parse</h3></p>
             <textarea class="input" name="xml" cols="40" rows="5"></textarea>
             <p><input type = 'submit' value = 'Parse'/></p>
          </form>
       </body>
    </html>
    """

# A5
@app.route('/admin', methods=['GET'])
def admin():
    return """
    <html>
       <body>
          <form action = "/admin" method = "GET">
             <p><h3>Admin Options</h3></p>
             <p><input type='submit' value='Delete User' name='pg_btn'/></p>
             <p><input type='submit' value='Create User' name='pg_btn'/></p>
             <p><input type='submit' value='Change User Permissions' name='pg_btn'/></p>
          </form>
       </body>
    </html>
    """

# A5
@app.route('/standard', methods=['GET'])
def standard():
    return """
    <html>
       <body>
          <form action = "/standard" method = "GET">
             <p><h3>Standard User Options</h3></p>
             <p><input type='submit' value='Change My Username' name='pg_btn'/></p>
          </form>
       </body>
    </html>
    """

# A5
"""
http://localhost/access
http://localhost/access?permissions=admin
"""
@app.route('/access', methods=['GET', 'POST'])
def access():
    if request.method == "GET":
        permissions=request.args.get('permissions')
        if permissions == "admin":
            return """
            <html>
               <body>
                  <form action = "/access" method = "POST">
                     <p><h3>Admin Choices</h3></p>
                     <p><input type='submit' value='Admin' name='pg_btn'/></p>
                     <p><input type='submit' value='Standard User' name='pg_btn'/></p>
                  </form>
               </body>
            </html>
            """
        else:
            return """
            <html>
               <body>
                  <form action = "/access" method = "POST">
                     <p><h3>Standard User Choices</h3></p>
                     <p><input type='submit' value='Standard User' name='pg_btn'/></p>
                  </form>
               </body>
            </html>
            """
    elif request.method == "POST":
        requested_page = request.form['pg_btn']
        if requested_page == "Admin":
            return redirect(url_for('admin')), 302
        elif requested_page == "Standard User":
            return redirect(url_for('standard')), 302

# A6
@app.route('/verbose', methods=['GET', 'POST'])
def verbose():
    if request.method == "GET":
        return """
        <html>
           <body>
              <form action = "/verbose" method = "POST">
                 <p><h3>Lookup User</h3></p>
                 <p><input type='text' value='Username' name='pg_txt'/></p>
                 <p><input type='submit' value='Find User' name='pg_btn'/></p>
              </form>
           </body>
        </html>
        """
    else:
        txt = request.form['pg_txt']
        try:
            # "
            # test" OR "1=1"--
            # username
            # ben.wireman
            req = 'select * from users where user_name = "' + txt + '"'
            with sqlite3.connect(_DB) as db_conn:
                db_cursor = db_conn.cursor()
                db_cursor.execute(req)
                rows = db_cursor.fetchall()
                if len(rows) > 0:
                    return """<html><body><p>User: """+str(rows)+"""</p></body></html>"""
                else:
                    return """<html><body><p>User doesn't exist</p></body></html>"""
        except sqlite3.Error as e:
            return """<html><body><p>Error: """+traceback.format_exc()+"""</p></body></html>"""
        except Exception as e:
            return """<html><body><p>Error: """+traceback.format_exc()+"""</p></body></html>"""

# A7
"""
http://localhost/tickets?q=test01
"""
@app.route('/tickets', methods=['GET', 'POST'])
def tickets():
    if request.method == 'GET':
        if request.args.get('q') is not None:
            tickets = None
            req = 'select * from tickets where ticket_title = "' + request.args.get('q') + '"'
            with sqlite3.connect(_DB) as db_conn:
                db_cursor = db_conn.cursor()
                db_cursor.execute(req)
                rows = db_cursor.fetchall()
                tickets = rows
            return """<html><body><p>"""+str(tickets)+"""</p></body></html>"""
        else:
            tickets = None
            req = 'select * from tickets'
            with sqlite3.connect(_DB) as db_conn:
                db_cursor = db_conn.cursor()
                db_cursor.execute(req)
                rows = db_cursor.fetchall()
                tickets = rows
                print(rows)
            cols = ['Title', 'Content', 'Assigned To']
            records = []
            for t in tickets:
                records.append(dict(zip(cols, t)))
            print(records)
            return render_template('Cross_Site_Scripting/tickets.html', records=records, colnames=cols)

    elif request.method == 'POST':
        req_ticket_title = request.form['pg_title']
        req_ticket_content = request.form['pg_content']
        req_ticket_user_name = request.form['pg_assigned']
        with sqlite3.connect(_DB) as db_conn:
            db_cur = db_conn.cursor()
            query = 'INSERT INTO tickets (ticket_title,ticket_content,user_name) VALUES (?,?,?)'
            db_cur.execute(query, [req_ticket_title,req_ticket_content,req_ticket_user_name])
            db_conn.commit()
        return redirect(url_for('tickets')), 302

# A8
@app.route('/cookie', methods = ['GET', 'POST'])
def cookie():
    cookieValue = None
    value = None
    pickled = None
    
    if request.method == 'POST':
        cookieValue = request.form['value']
        value = cookieValue
    if 'value' in request.cookies:
        pickled = pickle.loads(b64decode(request.cookies['value']))
    form = """
    <html>
       <body>
          <p>Cookie value: """ + str(cookieValue) +"""</p>
          <p>Pickled: """ + str(pickled) +"""</p>
          <p>Unpickled: """ + str(pickle.dumps(pickled)) +"""</p>
          <form action = "/cookie" method = "POST">
             <p><h3>Enter value to be stored in cookie</h3></p>
             <p><input type = 'text' name = 'value'/></p>
             <p><input type = 'submit' value = 'Set Cookie'/></p>
          </form>
       </body>
    </html>
    """
    resp = make_response(form)
    if value:
        resp.set_cookie('value', value)
    return resp

# A8
@app.route('/listener', methods = ['GET', 'POST'])
def listener():
    return jsonify({"message": "successful POST of data from web server...", "data": request.form['data']}), 200

# Pure unrestricted file upload
@app.route('/urupload', methods=['GET','POST'])
def urupload():
    if request.method == 'POST':
        if request.files:
            uploadedFile = request.files["file"]
            print(uploadedFile)
            uploadedFile.save(os.path.join("../lessons/File_Upload/uploads/", uploadedFile.filename))                                          
            return redirect(request.url)
    else:
        return """
        <html>
          <body>
            <h1> File Upload </h1>
            <form action="/urupload" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Select File</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file" id="file">
                        <label class="custom-file-label" for="file">Select file...</label>
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Upload</button>
            </form>
          </body>
        </html>
        """

# unrestricted file upload web shell
@app.route('/webshell', methods=['GET','POST'])
def web_shell():
    if request.method == 'POST':
        if request.files:
            uploadedFile = request.files["file"]
            print(uploadedFile)
            uploadedFile.save("./templates/File_Upload/" + uploadedFile.filename)
            return redirect(request.url)
    else:
        return render_template('File_Upload/upload.php')

if __name__ == "__main__":
    reset = True
    if reset:
        create_environment()
    app.run(host='localhost', port=80, debug=True)