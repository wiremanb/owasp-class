# A1 - Injection

#### Description: When an attacker can send data to an interpreter
#### Impact: Data loss, information disclosure, host compromise/take-over
